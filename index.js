console.log("Welcome, Arn Mendoza");

// Practice Javascript Coding Session Dec 12, 2022 Time finished: 3:46 pm

let num1 = 10;
let num2 = 7;
let sum = num1 + num2;

console.log("The Total Sum is: " + sum);

let num3 = 9;
let num4 = 8;
let multi = num3 * num4;

console.log("The Total Multiplication is: " + multi);

let num5 = 60;
let num6 = 3;	
let modul = num5 % num6;

console.log("The Total Mudolu is: " + modul);

const num7 = 10;
const num8 = 50;

let total1 = 9 * num7;

console.log(total1);

let total2 = num8 / num7;

console.log(total2);

let total3 = 5 + 10 * 3;

console.log(total3);

let total4 = num8 + num7 / 8 + 2;

console.log(total4);

// Types of Operators 


/*
	Operators	       Name				Example		Shortcut for
	
	+= 			Addition Assignment		x += 4  	x = x + 4;

	-=			Subtraction Assignment	x -= 3;		x = x - 3;

	*=			Multiplication			x *= 3;		x = x * 3;

	/=			Division				x /= 5; 	x = x / 5;

*/

let x = 3;
let y = 4;

let total5 = x + y;

console.log("The total sum using Javascript Math Operators: " + total5);

let x1 = 3;
let y1 = 4;

let total6 = y1 - x1;

console.log("The total subtraction using Javascript Math Operators: " + total6);

let x2 = 3;
let y2 = 4;

let total7 = y2 * x2;

console.log("The total multiplication using Javascript Math Operators: " + total7);

let x3 = 3;
let y3 = 4;

let total8 = x3 / y3;

console.log("The total division using Javascript Math Operators: " + total8);